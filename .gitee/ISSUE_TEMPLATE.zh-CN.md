### 架构与系统版本

| container | value          |
| --------- | -------------- |
| os        | ubuntu devel   |
| arch      | arm64          |
| type      | chroot/unshare |
| desktop   | gnome 41       |

---

| host     | value      |
| -------- | ---------- |
| os       | android 12 |
| arch     | arm64      |
| terminal | ?          |
| tmoe     | latest     |

---

### 该问题是怎么引起的

### 报错信息或截图
